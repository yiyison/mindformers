# Copyright 2025 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""test run check"""
import pytest
import json
import time
from unittest.mock import patch, MagicMock, mock_open
from pathlib import Path
import sys

# 导入你提供的 run_check 函数和相关的类
from mindformers.run_check import run_check, MSCheck, MFCheck, VersionCheck


@pytest.fixture
def mock_version_mapping():
    # 模拟 VERSION_MAP.json 内容
    return '''{
        "mf": {
            "1.0.0": {
                "prefer": "1.0.0",
                "support": ["0.9.0"]
            }
        },
        "ms": {
            "1.0.0": {
                "prefer": "1.0.0",
                "support": ["0.9.0"]
            }
        },
        "cann": {
            "1.0.0": {
                "prefer": "1.0.0",
                "support": ["0.9.0"]
            }
        }
    }'''


@pytest.fixture
def mock_run_check():
    # 模拟 run_check 相关的操作和返回值
    with patch(f"{__name__}.MSCheck.check") as mock_ms_check, \
            patch(f"{__name__}.MFCheck.check") as mock_mf_check, \
            patch(f"{__name__}.VersionCheck.check") as mock_version_check:
        mock_ms_check.return_value = None
        mock_mf_check.return_value = None
        mock_version_check.return_value = None

        yield mock_ms_check, mock_mf_check, mock_version_check


def test_run_check(mock_version_mapping, mock_run_check):
    # 模拟 VERSION_MAP.json 文件存在
    with patch("builtins.open", mock_open(read_data=mock_version_mapping)):
        # 运行检查
        run_check()

        # 验证 MSCheck, MFCheck 和 VersionCheck 是否按顺序被调用
        mock_ms_check, mock_mf_check, mock_version_check = mock_run_check

        # 检查是否按预期顺序执行
        mock_ms_check.assert_called_once()
        mock_version_check.assert_called_once()

        # 可以进一步检查每个调用的参数
        # 示例：检查 MSCheck 是否在运行后正确调用
        mock_ms_check.assert_called_with()


def test_run_check_with_missing_version_file(mock_run_check):
    # 模拟 VERSION_MAP.json 文件不存在
    with patch("builtins.open", mock_open(read_data=mock_version_mapping)):
        with pytest.raises(RuntimeError, match="Cannot find VERSION_MAP.json"):
            run_check()


def test_run_check_with_invalid_version_mapping(mock_run_check):
    # 模拟 VERSION_MAP.json 文件格式错误
    with patch("builtins.open", mock_open(read_data=mock_version_mapping)):
        with pytest.raises(json.JSONDecodeError):
            run_check()


# 其他模拟测试：测试 MSCheck, MFCheck 和 VersionCheck 中的具体行为
def test_ms_check_error_handling(mock_version_mapping, mock_run_check):
    # 模拟 MSCheck 中出现的错误
    with patch(f"{__name__}.MSCheck.check", side_effect=RuntimeError("MS Check Failed")):
        with pytest.raises(RuntimeError, match="The run check failed"):
            run_check()


def test_mf_check_error_handling(mock_version_mapping, mock_run_check):
    # 模拟 MFCheck 中出现的错误
    with patch(f"{__name__}.MFCheck.check", side_effect=RuntimeError("MF Check Failed")):
        with pytest.raises(RuntimeError, match="The run check failed"):
            run_check()


def test_version_check_error_handling(mock_version_mapping, mock_run_check):
    # 模拟 VersionCheck 中出现的错误
    with patch(f"{__name__}.VersionCheck.check", side_effect=RuntimeError("Version Check Failed")):
        with pytest.raises(RuntimeError, match="The run check failed"):
            run_check()

